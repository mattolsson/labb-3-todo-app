//
//  AddTaskViewController.m
//  To-Do
//
//  Created by MattiasO on 2015-02-08.
//  Copyright (c) 2015 Mattias Olsson. All rights reserved.
//

#import "AddTaskViewController.h"
#import "ToDoTableViewController.h"
#import "Task.h"

@interface AddTaskViewController ()
@property (weak, nonatomic) IBOutlet UITextField *textTask;
@end

@implementation AddTaskViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)addButtonPressed:(id)sender {
    [self.addArray addObject:self.textTask.text];
}
    
@end


