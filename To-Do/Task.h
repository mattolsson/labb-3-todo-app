//
//  Task.h
//  To-Do
//
//  Created by MattiasO on 2015-02-08.
//  Copyright (c) 2015 Mattias Olsson. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Task : NSObject

@property (nonatomic) NSString *title;

@end
