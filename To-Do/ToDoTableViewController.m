//
//  ToDoTableViewController.m
//  To-Do
//
//  Created by MattiasO on 2015-02-08.
//  Copyright (c) 2015 Mattias Olsson. All rights reserved.
//

#import "ToDoTableViewController.h"
#import "Task.h"
#import "AddTaskViewController.h"


@interface ToDoTableViewController ()
@property (nonatomic) NSMutableArray* addArray;

@end

@implementation ToDoTableViewController

- (void)viewWillAppear:(BOOL)animated {
    [self.tableView reloadData];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.addArray.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:
    @"MyCell" forIndexPath:indexPath];
    cell.textLabel.text = self.addArray[indexPath.row];
    
    return cell;
}

- (NSMutableArray*)addArray {
    if(!_addArray) {
        Task *t1 = [[Task alloc]init];
        t1.title = @"Tvätta";
        Task *t2 = [[Task alloc]init];
        t2.title = @"Diska";
        Task *t3 = [[Task alloc]init];
        t3.title = @"Städa";
        Task *t4 = [[Task alloc]init];
        t4.title = @"Handla";
        Task *t5 = [[Task alloc]init];
        t5.title = @"Plugga";
        
        self.addArray = [@[t1.title, t2.title, t3.title, t4.title, t5.title]mutableCopy];
    }
    return _addArray;
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    AddTaskViewController *addTask = [segue destinationViewController];
    addTask.addArray = self.addArray;
    }



// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}


// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        [_addArray removeObjectAtIndex:indexPath.row];
        
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        
    }   
}


/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/





@end
